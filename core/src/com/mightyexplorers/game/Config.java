package com.mightyexplorers.game;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class Config {

    public static Integer screenWidth = 1280;
    public static Integer screenHeigth = 720;
    public static String DefaultBackgroundMusic = "Music/background.wav";
    public static String DefaultClickSound = "Music/effects/click.wav";
}
