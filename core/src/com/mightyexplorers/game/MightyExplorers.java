package com.mightyexplorers.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mightyexplorers.game.service.FileService;
import com.mightyexplorers.game.entity.Fleet;
import com.mightyexplorers.game.screens.MainMenuScreen;

public class MightyExplorers extends Game {
	public SpriteBatch batch;
	Texture img;
	public BitmapFont font;
	public Music backgroundMusic;
	public Sound clicksound;
	public Boolean backgroundIsDrawn = false;
	public Fleet fleet;
	public Stage stage;
	public Skin skin;

	public void createFleet() {
		fleet = new Fleet();
	}

	public void create() {
		batch = new SpriteBatch();
		loadSoundAssets();
		font = new BitmapFont();
		skin = new Skin(Gdx.files.internal("gui/neon/skin/neon-ui.json"));
		//skin = new Skin(Gdx.files.internal("gui/default/skin/uiskin.json"));
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		this.setScreen(new MainMenuScreen(this));

	}

	@Override
	public void render() {
		super.render(); //important!
	}

	@Override
	public void dispose() {
		batch.dispose();
		stage.dispose();
		if(img != null) {
            img.dispose();
        }

        if(clicksound != null) {
            clicksound.dispose();
        }
	}

	public void loadSoundAssets() {
		clicksound = Gdx.audio.newSound(Gdx.files.internal(Config.DefaultClickSound));

	}

	public void playButtonClick() {
		clicksound.play(1.0f);
	}

	public Boolean saveGame() {
		if(FileService.save(fleet)){
			return true;
		}
		return false;
	}

	public Fleet loadGame () {
		Fleet retval = null;
		try {
			retval = (Fleet) FileService.load("someName");
		} catch (Exception e) {
			return null;
		}
		return retval;
	}

}
