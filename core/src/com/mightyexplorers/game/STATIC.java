package com.mightyexplorers.game;

/**
 * Created by Martijn Faber on 5-4-2018.
 */

public class STATIC {
    final static public String TXT_STARTGAME = "Start new game";
    final static public String TXT_CONTINUEGAME = "Continue game";
}
