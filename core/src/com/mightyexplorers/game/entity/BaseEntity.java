package com.mightyexplorers.game.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class BaseEntity {
    public Timestamp timestamp;
    public Rectangle position;
    public Texture texture;

    public BaseEntity() {
        Calendar calendar = Calendar.getInstance();
        timestamp = new java.sql.Timestamp(calendar.getTime().getTime());
    }

    public BaseEntity(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
