package com.mightyexplorers.game.entity;

import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.utils.Array;


/**
 * Created by Martijn Faber on 19-12-2017.
 */

public class Fleet {

    public Array<Ship> shipList;
    public List<CrewMember> crewMemberList;
    public Person captain;

    Integer crew;
    Integer food;
    Integer moral;

    public void createCaptain() {
        captain = new Person();
    }

    public void createFirstShipList() {
        shipList =  new Array<Ship>();
        shipList.add(new Ship());
    }

    public Fleet() {
        createCaptain();
        crew = 5;
        food = 50;
        moral = 100;
        createFirstShipList();
    }
}
