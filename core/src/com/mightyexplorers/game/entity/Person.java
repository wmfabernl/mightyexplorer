package com.mightyexplorers.game.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Martijn Faber on 19-12-2017.
 */

public class Person extends BaseEntity {
    String name;
    Integer health;

    public Person() {
        super();
        position = new Rectangle();
        position.x = 0;
        position.y = 200;
        position.width = 100;
        position.height = 100;
        texture = new Texture(Gdx.files.internal("images/entities/avatars/M_01_bg.png"));
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHealth (Integer hp) {
        this.health = hp;
    }
}
