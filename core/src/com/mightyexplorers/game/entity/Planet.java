package com.mightyexplorers.game.entity;

import com.badlogic.gdx.graphics.Texture;

import java.sql.Timestamp;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class Planet extends BaseEntity {
    Texture texture;
    Timestamp time;
    Moon moon;

    public Planet () {
        super();
    }

    public Planet (Timestamp timestamp) {
        super(timestamp);
    }


}
