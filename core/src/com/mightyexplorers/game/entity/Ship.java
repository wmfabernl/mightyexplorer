package com.mightyexplorers.game.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.mightyexplorers.game.Config;
import com.mightyexplorers.game.service.CalculationService;

import java.sql.Timestamp;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class Ship extends BaseEntity {
    public Timestamp time;

    public Ship() {
        super();
        position = new Rectangle();
        position.x = 0;
        position.y = CalculationService.getRandom(10,(Config.screenHeigth)-50);
        position.y = 200;
        position.width = 100;
        position.height = 100;
        texture = new Texture(Gdx.files.internal("images/entities/ship/Ship_Medium (7).png"));
    }


}
