package com.mightyexplorers.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.mightyexplorers.game.Config;
import com.mightyexplorers.game.MightyExplorers;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public abstract class BaseScreen implements Screen {

    final MightyExplorers game;
    Music backgroundMusic ;


    public BaseScreen(final MightyExplorers game) {
        this.game = game;
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("background.wav"));
        backgroundMusic.setLooping(true);
        backgroundMusic.play();

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void dispose() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }
}
