package com.mightyexplorers.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mightyexplorers.game.Config;
import com.mightyexplorers.game.MightyExplorers;
import com.mightyexplorers.game.service.BackgroundService;
import com.mightyexplorers.game.service.BorderService;
import com.mightyexplorers.game.service.CalculationService;
import com.mightyexplorers.game.entity.Ship;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class FlightScreen implements Screen {

    final MightyExplorers game;
    OrthographicCamera camera;
    Ship ship;
    Integer backgroundImageNumber;
    Rectangle side;
    Integer cruiseHeight;

    public FlightScreen(final MightyExplorers game) {
        this.game = game;
        this.game.batch = new SpriteBatch();
        this.game.backgroundIsDrawn = false;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Config.screenWidth, Config.screenHeigth);
        setNewVector();
    }


    void setNewVector() {
        ship = new Ship();
        side = BorderService.getLeftBorder();
        backgroundImageNumber = BackgroundService.getBackgroundNumber();
        cruiseHeight = CalculationService.getRandom(10,(Config.screenHeigth)-50);
    }

    @Override
    public void render(float delta) {
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        BackgroundService.drawSpaceBackgroundByNumber(game,backgroundImageNumber);
        game.batch.draw(ship.texture, ship.position.x, this.cruiseHeight,100,ship.texture.getHeight()/(ship.texture.getWidth()/100));
        ship.position.x += 300 * Gdx.graphics.getDeltaTime();
        game.batch.end();
        if(side.overlaps(ship.position)){
            setNewVector();
        }
    }

    @Override
    public void dispose() {
        //camera.dispose();
        //ship.dispose();
        //BackgroundImageNumber.dispose();
        //side.dispose();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }
}
