package com.mightyexplorers.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.mightyexplorers.game.Config;
import com.mightyexplorers.game.MightyExplorers;
import com.mightyexplorers.game.STATIC;
import com.mightyexplorers.game.service.BackgroundService;
import com.mightyexplorers.game.service.InputService;
import com.mightyexplorers.game.service.MusicService;
import com.mightyexplorers.game.service.PlanetService;


public class MainMenuScreen implements Screen {



    final MightyExplorers game;
    OrthographicCamera camera;

    public MainMenuScreen(final MightyExplorers game) {
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Config.screenWidth, Config.screenHeigth);
        this.game.backgroundMusic = MusicService.getBackgroundMusic();
        this.game.backgroundMusic.play();

    }

    @Override
    public void render(float delta) {
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.stage.addActor(getStartButton());
        game.stage.draw();

        game.batch.begin();

        if(this.game.backgroundIsDrawn == false) {
            BackgroundService.drawSpaceBackground(game);
            PlanetService.drawPlanet(game);
            this.game.backgroundIsDrawn = true;
        }


        game.batch.end();

    }

    public Button getStartButton() {
        InputListener inputListener = new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.playButtonClick();
                dispose();
                game.backgroundIsDrawn = false;
                game.stage.dispose();
                game.setScreen(new SetupScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        };
        return InputService.getStartButton(this.game.skin, STATIC.TXT_STARTGAME, inputListener);
    }



    @Override
    public void dispose() {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

}