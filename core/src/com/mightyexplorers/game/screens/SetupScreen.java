package com.mightyexplorers.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mightyexplorers.game.MightyExplorers;
import com.mightyexplorers.game.entity.Ship;
import com.mightyexplorers.game.service.BackgroundService;
import com.mightyexplorers.game.service.InputService;
import com.mightyexplorers.game.service.PersonService;
import com.mightyexplorers.game.service.PlanetService;
import com.mightyexplorers.game.service.ShipService;

public class SetupScreen implements Screen {

    final MightyExplorers game;

    Boolean redrawNextRound;
    Boolean firstDraw;
    Integer backgroundImageNumber;
    Table root;

    Table table;

    OrthographicCamera camera;
    String captainName;
    InputListener inputListener;

    TextField textfieldName;

    ButtonGroup buttonGroup2;

    ButtonGroup buttonGroup;

    public SetupScreen(final MightyExplorers game) {
        backgroundImageNumber = BackgroundService.getBackgroundNumber();
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        this.game.stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(this.game.stage);
        root = new Table();
        table = new Table();
        buildListener();
        buildMenu();
        buildScreen();

    }

    public void buildScreenWithParams () {
        if( firstDraw == true ) {
            camera.update();
            game.batch.setProjectionMatrix(camera.combined);

            game.stage.draw();
            game.stage.act();
            game.batch.begin();
            game.stage.addActor(root);
        }

        if(redrawNextRound == true) {
            refreshBackground();
        }

        if( firstDraw == true) {
            game.batch.end();
            firstDraw = false;
        }
    }

    public void buildScreen () {
        redrawNextRound = true;
        firstDraw = true;
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.stage.draw();
        game.stage.act();
        game.batch.begin();
        game.stage.addActor(root);

        if(redrawNextRound == true) {
            refreshBackground();
        }
        game.batch.end();
    }


    @Override
    public void render(float delta) {
        if(game.fleet == null) {
            game.createFleet();
        }
        buildScreenWithParams();
    }

    void buildListener() {
         inputListener = new InputListener() {

            public boolean touchDown (InputEvent event, float x, float y,
                                      int pointer, int button) {
                buildScreen();
                return true;
            }

            public void touchUp (InputEvent event, float x, float y,
                                 int pointer, int button) {

            }

             public boolean keyUp (InputEvent event, int keycode) {
                buildScreen();
                return true;
             }

        };
    }

    void refreshBackground() {
        BackgroundService.drawSpaceBackgroundByNumber(game, backgroundImageNumber);
        PlanetService.drawPlanet(game);
        this.game.backgroundIsDrawn = true;
        redrawNextRound = false;
    }

    void buildMenu() {
        root.setFillParent(true);
        table.defaults().growX();
        table.add(getInputNameTextLabel());
        table.add(getInputNameTextField());
        table.row();
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(ShipService.getMediumShipTexture("4"))))).width(200).height(200).pad(10);
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(ShipService.getMediumShipTexture("5"))))).width(200).height(200).pad(10);
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(ShipService.getMediumShipTexture("6"))))).width(200).height(200).pad(10);
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(ShipService.getMediumShipTexture("7"))))).width(200).height(200).pad(10);
        table.row();
        buttonGroup = new ButtonGroup();
        buttonGroup.setMaxCheckCount(1);
        buttonGroup.setMinCheckCount(0);
        buttonGroup.setUncheckLast(true);

        ImageTextButton radio1 = new ImageTextButton("Ship 1", game.skin, "radio");
        radio1.addListener(inputListener);
        table.add(radio1);
        buttonGroup.add(radio1);

        ImageTextButton radio2 = new ImageTextButton("Ship 2", game.skin, "radio");
        radio2.addListener(inputListener);
        table.add(radio2);
        buttonGroup.add(radio2);

        ImageTextButton radio3 = new ImageTextButton("Ship 3", game.skin, "radio");
        radio3.addListener(inputListener);
        table.add(radio3);
        buttonGroup.add(radio3);

        ImageTextButton radio4 = new ImageTextButton("Ship 4", game.skin, "radio");
        radio4.addListener(inputListener);
        table.add(radio4);
        buttonGroup.add(radio4);

        table.row();

        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(PersonService.getPersonTexture("F_09"))))).width(200).height(200).pad(10);
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(PersonService.getPersonTexture("M_01"))))).width(200).height(200).pad(10);
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(PersonService.getPersonTexture("A_05"))))).width(200).height(200).pad(10);
        table.add(new ImageButton(new TextureRegionDrawable(new TextureRegion(PersonService.getPersonTexture("K_03"))))).width(200).height(200).pad(10);
        table.row();

        buttonGroup2 = new ButtonGroup();
        buttonGroup2.setMaxCheckCount(1);
        buttonGroup2.setMinCheckCount(0);
        buttonGroup2.setUncheckLast(true);

        ImageTextButton radio5 = new ImageTextButton("Avatar 1", game.skin, "radio");
        radio5.addListener(inputListener);
        table.add(radio5);
        buttonGroup2.add(radio5);

        ImageTextButton radio6 = new ImageTextButton("Avatar 2", game.skin, "radio");
        radio6.addListener(inputListener);
        table.add(radio6);
        buttonGroup2.add(radio6);

        ImageTextButton radio7 = new ImageTextButton("Avatar 3", game.skin, "radio");
        radio7.addListener(inputListener);
        table.add(radio7);
        buttonGroup2.add(radio7);

        ImageTextButton radio8 = new ImageTextButton("Avatar 4", game.skin, "radio");
        radio8.addListener(inputListener);
        table.add(radio8);
        buttonGroup2.add(radio8);
        table.row();

        table.add(createStartGameButton());
        root.add(table);
    }


    private TextButton createStartGameButton () {
        InputListener inputListener = new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.playButtonClick();
                dispose();
                game.backgroundIsDrawn = false;
                game.stage.dispose();
                game.createFleet();
                captainName = textfieldName.getText();
                game.fleet.captain.setName(captainName);
                Integer chosenShipIdex = buttonGroup.getCheckedIndex();
                Integer chosenAvatarIdex = buttonGroup2.getCheckedIndex();

                //Ship ship = ShipService.getMediumShip();
                //game.fleet.shipList.add(ship);
                game.saveGame();
                //game.setScreen(new SetupScreen(game));
                game.setScreen(new FlightScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        };
        return InputService.getButton(this.game.skin , "Start Game", inputListener, 175, 100);
    }

    public Label getInputNameTextLabel () {
        Label label = new Label("Captain name",game.skin);
        label.setFontScale(2f);
        return label;
    }

    public TextField getInputNameTextField () {
        textfieldName = InputService.getTextField(game.skin,"", 400, 850);
        textfieldName.addListener(inputListener);
        return textfieldName;
    }

    @Override
    public void dispose() {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }
}

