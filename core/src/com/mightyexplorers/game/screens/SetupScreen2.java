package com.mightyexplorers.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mightyexplorers.game.MightyExplorers;
import com.mightyexplorers.game.service.BackgroundService;
import com.mightyexplorers.game.service.InputService;
import com.mightyexplorers.game.service.PersonService;
import com.mightyexplorers.game.service.PlanetService;
import com.mightyexplorers.game.service.ShipService;
import com.mightyexplorers.game.entity.Person;
import com.mightyexplorers.game.entity.Ship;

public class SetupScreen2 implements Screen {

    final MightyExplorers game;

    TextButton button;
    TextButton.TextButtonStyle textButtonStyle;
    BitmapFont font;
    Skin skin;
    TextureAtlas buttonAtlas;

    OrthographicCamera camera;

    public SetupScreen2(final MightyExplorers game) {
        this.game = game;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        this.game.stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(this.game.  stage);
    }

    @Override
    public void render(float delta) {
        if(game.fleet == null) {
           game.createFleet();
        }
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        TextField textField = getInputNameTextField();
        //textField.setName(game.fleet.captain.getName());
        TextButton startButton = this.createStartGameButton();

        game.stage.addActor(getInputNameTextLabel());
        game.stage.addActor(textField);
        game.stage.addActor(getFilledInTextLabel());
        game.stage.addActor(startButton);
        game.stage.draw();
        game.stage.act();

        game.batch.begin();

        Ship ship = ShipService.getMediumShip("6");
        game.batch.draw(ship.texture, 75, 275,100,ship.texture.getHeight()/(ship.texture.getWidth()/100));

        Ship ship2 = ShipService.getMediumShip("7");
        game.batch.draw(ship2.texture, 200, 275,100,ship.texture.getHeight()/(ship2.texture.getWidth()/100));

        Ship ship3 = ShipService.getMediumShip("8");
        game.batch.draw(ship3.texture, 325, 275,100,ship.texture.getHeight()/(ship3.texture.getWidth()/100));

        Ship ship4 = ShipService.getMediumShip("4");
        game.batch.draw(ship4.texture, 450, 275,100,ship.texture.getHeight()/(ship4.texture.getWidth()/100));

        Person person = PersonService.getPerson("F_09");
        game.batch.draw(person.texture, 75, 125,100,person.texture.getHeight()/(person.texture.getWidth()/100));

        Person person2 = PersonService.getPerson("M_01");
        game.batch.draw(person2.texture, 200, 125,100,person.texture.getHeight()/(person.texture.getWidth()/100));

        Person person3 = PersonService.getPerson("A_05");
        game.batch.draw(person3.texture, 325, 125,100,person.texture.getHeight()/(person.texture.getWidth()/100));

        Person person4 = PersonService.getPerson("K_03");
        game.batch.draw(person4.texture, 450, 125,100,person.texture.getHeight()/(person.texture.getWidth()/100));




        if(this.game.backgroundIsDrawn == false) {
            BackgroundService.drawSpaceBackground(game);
            PlanetService.drawPlanet(game);
            this.game.backgroundIsDrawn = true;
        }


        game.batch.end();

    }

    private TextButton createStartGameButton () {
        InputListener inputListener = new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.playButtonClick();
                dispose();
                game.backgroundIsDrawn = false;
                game.stage.dispose();
                game.createFleet();
                game.setScreen(new FlightScreen(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        };
        return InputService.getButton(this.game.skin , "Start Game", inputListener, 175, 100);
    }

    public Label getInputNameTextLabel () {
        Label label = new Label("Captain name",game.skin);
        label.setPosition(200,850);
        label.setFontScale(1f);
        return label;
    }

    public Label getFilledInTextLabel () {
        Label label = new Label(game.fleet.captain.getName(),game.skin);
        label.setPosition(600,850);
        label.setFontScale(1f);
        return label;
    }

    public TextField getInputNameTextField () {
        TextField textfield = InputService.getTextField(game.skin,"", 400, 850);
        return textfield;
    }

    /*public TextField.TextFieldListener getNameInputListener() {
        TextField.TextFieldListener textInputListener = new TextField.TextFieldListener() {
            @Override
            public void keyTyped(final TextField textField, final char character) {
                System.out.println(character);
            }
        };
        return textInputListener;
    }*/


    @Override
    public void dispose() {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }
}

