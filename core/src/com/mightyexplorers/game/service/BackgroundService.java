package com.mightyexplorers.game.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mightyexplorers.game.Config;
import com.mightyexplorers.game.MightyExplorers;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class BackgroundService {

    public static Integer getBackgroundNumber() {
        return CalculationService.getRandom(1,59);
    }



    public static void drawSpaceBackground (MightyExplorers game) {
        Texture texture1 = new Texture(Gdx.files.internal("images/background/Space_background_"+ CalculationService.getRandom(1,59)+".png"));
        game.batch.draw(texture1,0,0,Config.screenWidth,Config.screenHeigth);
    }

    public static void drawSpaceBackground (MightyExplorers game, Integer backgroundNumber) {
        drawSpaceBackgroundByNumber (game, backgroundNumber);
    }

    public static void drawSpaceBackgroundByNumber (MightyExplorers game, Integer backgroundNumber) {
        Texture texture1 = new Texture(Gdx.files.internal("images/background/Space_background_"+ backgroundNumber +".png"));
        game.batch.draw(texture1,0,0,Config.screenWidth,Config.screenHeigth);
    }


}
