package com.mightyexplorers.game.service;

import com.badlogic.gdx.math.Rectangle;
import com.mightyexplorers.game.Config;

/**
 * Created by Martijn Faber on 19-12-2017.
 */

public class BorderService {

    public static Rectangle getLeftBorder () {
        Rectangle side = new Rectangle();
        side.x = Config.screenWidth+100;
        side.y = 0;
        side.height = Config.screenHeigth;
        side.width = Config.screenWidth;
        return side;
    }
}
