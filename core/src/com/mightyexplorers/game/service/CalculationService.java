package com.mightyexplorers.game.service;

import java.util.Random;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class CalculationService {

    public static Integer getRandom (Integer min, Integer max) {
        Random r = new Random();

        Integer value = r.nextInt((max - min) + 1) + min;
        return value;
    }
}
