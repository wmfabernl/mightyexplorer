package com.mightyexplorers.game.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

/**
 * Created by Martijn Faber on 5-4-2018.
 */

public class InputService {

    private static int row_height = Gdx.graphics.getWidth() / 12;
    private static int col_width = Gdx.graphics.getWidth() / 12;

    public static Integer getTopLeftX () {
        return InputService.col_width*2;
    }

    public static Integer getTopLeftY() {
        return Gdx.graphics.getHeight()- InputService.row_height*2;
    }

    public static TextButton getStartButton (Skin skin, String buttonText, InputListener inputListener) {
        return getButton(skin, buttonText, inputListener, getTopLeftX(), getTopLeftY());
    }

    public static TextButton getButton (Skin skin, String buttonText, InputListener inputListener, Integer X, Integer Y) {
        TextButton button2 = new TextButton(buttonText, skin);
        //button2.getLabel().setFontScale(1f,1f);
        button2.setSize(InputService.col_width*3, InputService.row_height);
        button2.setPosition(X,Y);
        button2.addListener(inputListener);
        return button2;
    }

    public static TextField getTextField(Skin skin,String textFieldString,Integer X ,Integer Y) {

        TextField textField = new TextField(textFieldString, skin);
        textField.setSize(InputService.col_width*3, InputService.row_height);
        textField.setPosition(X ,Y);
        return textField;
    }
}
