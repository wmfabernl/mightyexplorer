package com.mightyexplorers.game.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.mightyexplorers.game.Config;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class MusicService {

    public static Music getBackgroundMusic() {
        Music backgroundMusic = getMusic(Config.DefaultBackgroundMusic);
        backgroundMusic.setLooping(true);
        return backgroundMusic;
    }

    public static Music getMusic(String filename) {
        return Gdx.audio.newMusic(Gdx.files.internal(filename));

    }





}
