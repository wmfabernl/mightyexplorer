package com.mightyexplorers.game.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mightyexplorers.game.entity.Person;

/**
 * Created by Martijn Faber on 5-4-2018.
 */

public class PersonService {

    public static Person getPerson (String identifier) {
        Person person = new Person();
        person.texture = getPersonTexture(identifier);
        return person;
    }


    public static TextureRegion getPersonTextureRegion (String identifier) {
        return new TextureRegion(getPersonTexture(identifier));
    }

    public static Texture getPersonTexture (String identifier) {
        Texture texture = new Texture(Gdx.files.internal("images/entities/avatars/"+identifier+"_bg.png"));
        return texture;
    }
}
