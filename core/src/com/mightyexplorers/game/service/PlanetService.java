package com.mightyexplorers.game.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mightyexplorers.game.Config;
import com.mightyexplorers.game.MightyExplorers;
import com.mightyexplorers.game.entity.Planet;

import java.sql.Timestamp;

/**
 * Created by Martijn Faber on 13-12-2017.
 */

public class PlanetService {

    public static Texture getPlanet (MightyExplorers game) {
        return new Texture(Gdx.files.internal("images/entities/planet/Planet ("+ CalculationService.getRandom(1,256)+").png"));


    }

    public static void drawPlanet (MightyExplorers game) {
        Texture planet = getPlanet(game);
        Integer size = CalculationService.getRandom(250,400);
        game.batch.draw(planet,Config.screenWidth/3*2,Config.screenHeigth/2, size, size);
    }

    public Planet createPlanet (MightyExplorers game) {
        Planet planet = new Planet();
        return planet;

    }

    public Planet createPlanet (MightyExplorers game, Timestamp timestamp) {
        Planet planet = new Planet(timestamp);
        return planet;
    }
}
