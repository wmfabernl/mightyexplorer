package com.mightyexplorers.game.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mightyexplorers.game.entity.Ship;

/**
 * Created by Martijn Faber on 5-4-2018.
 */

public class ShipService {
    public static Ship getMediumShip (String identifier) {
        Ship ship = new Ship();
        ship.texture = getMediumShipTexture(identifier);
        return ship;
    }

    public static TextureRegion getMediumShipTextureRegion (String identifier) {
        return new TextureRegion(getMediumShipTexture(identifier));
    }

    public static Texture getMediumShipTexture (String identifier) {
        Texture texture = new Texture(Gdx.files.internal("images/entities/ship/Ship_Medium ("+identifier+").png"));
        return texture;
    }
}
